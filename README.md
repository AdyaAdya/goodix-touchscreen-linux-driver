# Goodix Touchscreen Linux Driver

With added support for Active Stylus Pen
---

This is a clone of the `drivers/input/touchscreen/goodix.c` file from the Linux mainline source code with a few modifications to **add support for the Goodix active pen**. (see `goodix_mainline.c` for the original driver)

*(Here the link to the [forum thread](https://bbs.archlinux.org/viewtopic.php?pid=1863262) where the issue was depicted)*

Table of contents
---
1. [List of known supported devices](#known-supported-devices)
2. [Build instructions](#build-the-driver)
3. [Installation instructions](#install-the-driver)
4. [Troubleshooting guide](#troubleshooting)
    - [Resolution information missing for libinput](#resolution-information-missing-for-libinput)
    - [Incorrect magic number after system update](#incorrect-magic-number-after-system-update)
5. [Why and what is this driver](#Why-and-what-is-this-driver)
6. [Credits](#credits)

---

#### Known supported devices
Here are the devices on which the driver has been tested and reported as working properly:
- **Chuwi**
  - Hi13
  - MiniBook
  - Surbook
  - UBook
- **Jumper**
  - EZPad Go
- **Microtech**
  - e-tab Pro
- **One Netbook**
  - One Mix 1S
  - One Mix 2S
- **Teclast**
  - F6 Pro Laptop
  - X4 2 in 1 Laptop Tablet 

*(feel free to let me know if it works on other devices)*

---

# Build the driver

Open a terminal and follow these steps:

1. **Clone** this repository:
  ```
    $ git clone https://gitlab.com/AdyaAdya/goodix-touchscreen-linux-driver
  ```

2. **Move** into the folder:
  ```
    $ cd goodix-touchscreen-linux-driver
  ```

3. If you want to **customize** the driver, a few parameters can be tweaked. To do so, open the `goodix.c` file in a text editor and jump to somewhere around the **66th line**. There you will find some `#define` statements which you might want to customize to fit your specific needs (for instance if you need to flip or rotate the screen; see `corrections.pdf`) :
  ```
    #define GOODIX_COORDINATE_CORRECTION 0
  ```

4. **Build** the driver:
  ```
    $ make
  ```

5. If no error occurs, **test** the driver (you need to have root privileges):
  ```
    # rmmod goodix
    # insmod goodix.ko
  ```

If the touchscreen and the pen work as you want them to, install the driver on your machine by following the steps below.

---

# Install the driver

Assuming you are in the cloned directory and you already successfully built the driver, follow these steps:

1. **Compress** into xz format the driver:
  ```
    $ xz goodix.ko
  ```

2. **Replace** the current driver with the new one (root privileges required):
  ```
    # mv goodix.ko.xz /lib/modules/$(uname -r)/kernel/drivers/input/touchscreen/goodix.ko.xz
  ```

It should be up and working now, even after reboot.

---

# Troubleshooting

If you are having trouble getting the driver to work, you might want to check the results of some of the following commands:
  ```
    $ dmesg | grep -i goodix
    $ cat /proc/bus/input/devices | grep -B 1 -A 8 -i goodix
    # libinput list-devices | grep -A 17 -i goodix
    # libinput debug-events
    $ xinput list
  ```


The following is a list of common issues you might encounter :

### Resolution information missing for libinput
- **Message** from the output of `# libinput debug-events`:
```
  eventX - Goodix Active Stylus Pen: libinput bug: missing tablet capabilities: resolution.  Ignoring this device
```
- **Reason**: This error is very likely to occur, if you are using `libinput` of course. It indicates that `udev` misses some resolution information for the pen device and thus cannot present it as a Tablet device.
- **Solution**:  **Update the `hwdb`** (HardWare DataBase) of your machine to add the missing resolution information by following these steps:
  1. **Locate** the `evdev.hwdb` file (should be in `/usr/lib/udev/hwdb.d/` or `/etc/udev/hwdb.d/`):
    ```
      # find / -type f -name *-evdev.hwdb
    ```
  2. **Edit** the file by appending the following (replace `2736` by the your horizontal resolution in pixels and `1824` by your vertical resolution, if necessary):
    ```
      # Goodix Touchscreen and Pen
      evdev:name:*Goodix*
        EVDEV_ABS_00=::2736
        EVDEV_ABS_01=::1824
        EVDEV_ABS_35=::2736
        EVDEV_ABS_36=::1824
    ```
  3. **Update** the hwdb:
    ```
      # systemd-hwdb update
    ```
  4. **Apply** the changes:
    ```
      # udevadm trigger
    ```
  5. **Reload** the driver (see step 4 of `Build the driver` section) (alternatively, you can reboot if you followed the steps of the `Install the driver` section)


### Incorrect magic number after system update

- **Message** from the output of `$ dmesg | grep -i goodix`:
```
  magic number "..." should be "..."
```
- **Reason**: The driver has not been properly compiled to be usable on your system. It might happen when you update the Linux kernel or the Linux kernel headers.
- **Solution**:
  1. **Reboot** your system to make sure you are using the (potentially newly) installed kernel :
  ```
    $ reboot
  ```

  2. **Move into the build directory** and **clean it up** by running the following inside of it:
  ```
    $ make clean
  ```

  3. **Copy** the `Module.symvers` file from your installed kernel build directory to the directory where you are building the driver. Assuming your working directory is that latter-mentionned directory :
  ```
    $ cp /lib/modules/$(uname -r)/build/Module.symvers .
  ```

  4. **Build**, **test** and **install** the driver by following the steps described in the `Build the driver` and `Install the driver` sections.

---

# Why and what is this driver

In this section I will try to explain the problems encountered and the train of thought that I followed to overcome them.

#### The initial problems

With the mainline driver, the input events generated by **a finger or a pen were not differentiated** which led to spurious events reported to the system.

- When the pen comes sufficiently close to the screen without coming into contact with it, the driver is expected to report a Proximity event for the Pen tool. However, it **instead reports a Touch event just as if a finger was pressing down on the screen** which makes the pen utterly unusable.

- Moreover, the pen and the touchscreen **share a single device** (of the form `/dev/input/eventX`).
  *Note*: if we report the Tool type to the system correctly, one might actually want them to share a single device (to simulate a graphics tablet that support finger events in addition to pen events) and thus might not consider this as a problem. But trying to do so led me to having the touchscreen not respond as well as it did before: the libinput events triggered by finger touch were definitely what you'd expect them to be.

#### The reason

The reason behind this is very simple: the **mainline driver code does not check the Tool type** of the touch report. There is a specific bit in the input data read from the touchscreen controller that is set to 1 when the input event is detected to be triggered by an active pen, but the mainline driver does not utilize it and only report events as `MT_TOOL_FINGER`.

#### The solutions

- For the reasons exposed in the "*Note*" above, I decided to **split the physical device into two logical devices** at the kernel driver level:
  - One for the touchscreen, which:
    - reports events triggered by **fingers only** ;
    - is identified as a **Touchscreen device** by `libinput` that generates events such as `TOUCH_DOWN`, `TOUCH_UP`, `TOUCH_MOTION`, `TOUCH_FRAME`, etc.
      *(this is the same as if we were using the mainline driver which works perfectly as far as finger events are concerned)*
  - One for the pen, which:
    - reports events triggered by the **pen only** ;
    - is identified as a **Tablet device** by `libinput` that generates events such as `TABLET_TOOL_PROXIMITY`, `TABLET_TOOL_AXIS`, `TABLET_TOOL_TIP`, etc.

- Now, to solve the first problem, we just need to **check whether the bit that indicates the Tool type is set or not** in the input data read from the TS controller. If it is set, we report the event to the Pen logical device, and if it isn't, to the Touchscreen logical device.

---

# Credits

Credit goes mostly to the orignal driver developers as well as the Goodix developers behind the driver for Android (at https://github.com/goodix/gt9xx_driver_android) which I did not manage to port to x86/x86_64 architecture. The code in `goodix.c` of my repository is basically a merge of the two from the mainline and that android driver with a few additions of mine. This repository is thus still under the **GPLv2 License**.
